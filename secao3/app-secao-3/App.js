import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Alerta from './src/components/Alerta/Alerta';

export default function App() {

  const [ escondeTelaLoad, setEscondeTelaLoad ] = useState(false);

  useEffect(() => {
    setEscondeTelaLoad(false);
    const timeout = setTimeout(() => {
      // escondendo a tela de load após 3 segundos
      setEscondeTelaLoad(true);
    }, 3000);
    // console.log(escondeTelaLoad);
    return () => clearTimeout(timeout);
  }, []);

  const telaLoad = () => {
    
    return <View style={ styles.estiloTelaLoad }>
      <Image
      style={ styles.estiloIconeLoad }
      source={ require('./assets/load.gif') } />
    </View>
  };

  return (
    /**
     * O SafeAreaView é um componente que equivale a uma div
     * do html, ele adequa o componente de tela 
     * ao android e ao IOS.
     */
    <SafeAreaView style={[ styles.container, { marginTop: getStatusBarHeight() } ]}>
      <StatusBar 
      barStyle='default'
      backgroundColor='rgba(236, 240, 241, 1.0)'
      />
      <Alerta />
      <Text style={ styles.estiloTextoExemplo }>
        Exemplo de componente!
      </Text>
      { escondeTelaLoad === false ? telaLoad() : null }
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  estiloTextoExemplo: {
    fontSize: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: 'red'
  },
  estiloTelaLoad: {
    position: 'absolute',
    zIndex: 999999,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(192, 57, 43, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  estiloIconeLoad: {
    width: 100,
    height: 100
  }
});
